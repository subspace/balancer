#ifndef _SERVER_H_
#define _SERVER_H_

#include <QHostAddress>

struct Server {
    QHostAddress addr;
    quint16 port;
        
    bool operator==(const Server& other) const {
        if (other.addr.toIPv4Address()  == this->addr.toIPv4Address() && other.port == this->port)
            return true;
        return false;
    }
};

#endif










