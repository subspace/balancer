#ifndef QUERYMASTER_H
#define QUERYMASTER_H

#include <QList>
#include "server.h"

class QUdpSocket;
class QTimer;

class QueryMaster: public QObject
{
    Q_OBJECT

public:
    QueryMaster(QObject* parent = 0);
    ~QueryMaster();

    void query(const QHostAddress& addr, quint16 port);
    
signals:
    void receivedList(const QList<Server>& serverList); /* Emmited when we receive a list of servers */
    void finished();            /* Emmited when the query has finished all its requests */
    
private slots:
    void readyRead();
    void socketTimedOut();

private:
    QList<QUdpSocket *> activeQueries;
    QList<QTimer *> activeTimers;

    void removeTimerBySocket(const QUdpSocket* socket);
};

#endif
