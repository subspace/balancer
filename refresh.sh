#!/bin/bash
cd $(dirname ${0})

export LD_LIBRARY_PATH=~/qt-everywhere-opensource-src-5.3.0/qtbase/lib

wget http://www.quakeservers.net/lists/servers/global.txt -O servers.txt
./balancer
cp new_cimsqwbot.cfg ~/qwbot/cimsqwbot.cfg
killall cimsqwbot               # The bot should restart by itself with a new list
