#include "queryserver.h"
#include <QUdpSocket>
#include <QString>
#include <QTimer>
#include <QDebug>

QueryServer::QueryServer(QObject *parent): QObject(parent)
{
}

QueryServer::~QueryServer()
{
}

void QueryServer::query(const QHostAddress &addr, quint16 port)
{
    // Query
    QUdpSocket* s = new QUdpSocket(this);
    connect(s, &QUdpSocket::readyRead, this, &QueryServer::parseServerInfo);
    s->writeDatagram(QByteArray("\xff\xff\xff\xffstatus 23\n"), addr, port);
    activeQueries.push_back(s);

    // Activate a timeout for this socket
    QTimer* tim = new QTimer(this);
    tim->setProperty("socket", qVariantFromValue((void *)s));
    tim->setInterval(1000*1);
    QObject::connect(tim, &QTimer::timeout, this, &QueryServer::socketTimedOut);
    tim->start();
    activeTimers.push_back(tim);
}

void QueryServer::removeTimerBySocket(const QUdpSocket* socket)
{
    foreach (QTimer* t, activeTimers) {
        if ((QUdpSocket*)t->property("socket").value<void *>() == socket) {
            activeTimers.removeAll(t);
            t->stop();
            t->deleteLater();
        }
    }
}

void QueryServer::parseServerInfo()
{
    QUdpSocket* s = qobject_cast<QUdpSocket *>(sender());
    QByteArray d;

    removeTimerBySocket(s);     // Remove the timer associated with this socket

    while (s->hasPendingDatagrams()) {
        QHostAddress addr;
        quint16 port;

        d.resize(s->pendingDatagramSize());
        s->readDatagram(d.data(), d.size(), &addr, &port);

        if (!d.startsWith("\xff\xff\xff\xffn")) {
            continue;
        }

        // Parse the server rules only
        QString     infoString(d.data()+5);
        QStringList splitInfoString = infoString.split("\n", QString::SkipEmptyParts);
        QStringList rules = splitInfoString.at(0).split("\\", QString::SkipEmptyParts);
        if((rules.size() % 2) != 0) {
            continue;
        }

        QPair<QString, QString> rule;
        QString hostname;
        bool valid = false;
        bool nospec = false;    // No spec slots for the bot
        for (int i = 0; i < rules.size(); i += 2) {
            rule.first  = rules.at(i);
            rule.second = rules.at(i+1);

            if (rule.first == "hostname") {
                hostname = rule.second;
                continue;
            }

            if (rule.first == "ktxver") {
                valid = true;
                continue;
            }

            if (rule.first == "CAC_D") { // CACE servers are ok
                valid = true;
                continue;
            }

            if (rule.first == "*gamedir" && (rule.second == "qw" || rule.second == "prox" || rule.second == "arena")) {
                valid = true;
                continue;
            }

            if (rule.first == "maxspectators" && rule.second.toUShort() == 0) { // No empty spec slots
                nospec = true;
                continue;
            }
        }
        if (valid && !nospec) {
            qDebug() << "[*] added" << hostname;
            emit receivedValidServer(addr, port);
        } else {
            if (nospec)
                qDebug() << "[*] removed" << hostname << "(no spec slots)";
            else
                qDebug() << "[*] removed" << hostname;
        }
    }

    activeQueries.removeAll(s);
    s->deleteLater();
    if (!activeQueries.size())
        emit finished();
}

void QueryServer::socketTimedOut()
{
    QTimer *tim = qobject_cast<QTimer *>(sender());
    QUdpSocket *s = qobject_cast<QUdpSocket *>((QUdpSocket *)tim->property("socket").value<void *>());

    qDebug() << "[*] timedout" << s->peerAddress().toString() << s->peerPort();

    activeQueries.removeAll(s);
    activeTimers.removeAll(tim);

    s->deleteLater();
    tim->deleteLater();

    if (!activeQueries.size())
        emit finished();
}
