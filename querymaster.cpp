#include "querymaster.h"
#include <QUdpSocket>
#include <QByteArray>
#include <QDataStream>
#include <QList>
#include <QTimer>
#include <QDebug>

QueryMaster::QueryMaster(QObject* parent):
    QObject(parent)
{
}

QueryMaster::~QueryMaster()
{
}

void QueryMaster::query(const QHostAddress& addr, quint16 port)
{
    QUdpSocket *s = new QUdpSocket(this);
    QByteArray d("c\n\0", 3);

    QObject::connect(s, &QUdpSocket::readyRead, this, &QueryMaster::readyRead);
    s->writeDatagram(d, addr, port);
    activeQueries.push_back(s);

    QTimer* tim = new QTimer(this);
    tim->setProperty("socket", qVariantFromValue((void *)s));
    tim->setInterval(1000*10);
    QObject::connect(tim, &QTimer::timeout, this, &QueryMaster::socketTimedOut);
    tim->start();
    
    activeTimers.push_back(tim);
}

void QueryMaster::removeTimerBySocket(const QUdpSocket* socket)
{
    foreach (QTimer* t, activeTimers) {
        if ((QUdpSocket*)t->property("socket").value<void *>() == socket) {
            activeTimers.removeAll(t);
            t->stop();
            t->deleteLater();
        }
    }
}

void QueryMaster::readyRead()
{
    QUdpSocket *s = qobject_cast<QUdpSocket*>(QObject::sender());
    QByteArray d;
    QHostAddress host;
    quint16 port;

    removeTimerBySocket(s);
    
    while (s->hasPendingDatagrams()) {
        d.resize(s->pendingDatagramSize());
        s->readDatagram(d.data(), d.size(), &host, &port);
        
        if (!d.startsWith("\xff\xff\xff\xff\x64\x0a")) {
            continue;
        }

        QDataStream is(d);
        is.setByteOrder(QDataStream::BigEndian);
        is.skipRawData(6);
        
        quint32 ip;
        QList<Server> l;
        
        while (!is.atEnd()) {
            is >> ip;
            is >> port;
            
            Server sv;
            sv.addr.setAddress(ip);
            sv.port = port;
            l.push_back(sv);
        }
        emit receivedList(l);
    }
    activeQueries.removeAll(s);
    s->deleteLater();

    if (!activeQueries.size())
        emit finished();
}

void QueryMaster::socketTimedOut()
{
    QTimer *tim = qobject_cast<QTimer *>(sender());
    QUdpSocket *s = qobject_cast<QUdpSocket *>((QUdpSocket *)tim->property("socket").value<void *>());

    activeQueries.removeAll(s);
    activeTimers.removeAll(tim);

    s->deleteLater();
    tim->deleteLater();
    
    if (!activeQueries.size())
        emit finished();
}








