#ifndef _BALANCER_H_
#define _BALANCER_H_

#include <QCoreApplication>
#include <QList>
#include "querymaster.h"

class Balancer: public QCoreApplication
{
    Q_OBJECT
public:
    Balancer(int &argc, char **argv);
    ~Balancer();

    void start();
    
    

private slots:
    void addListOfServers(const QList<Server> &list);
    void startQueryingServers();
    void addValidServer(const QHostAddress& addr, quint16 port);
    void queryNextServer();
    void quitApplication();
    
private:
    QList<Server> allServers;
    QList<Server> validServers;
    QList<Server> blackListedServers; // Servers that are forcebly removed from the list
    QList<Server> masterServers;
    
    void generateConfig();
    void loadBlackListFromFile(const QString& filePath);
    void loadMastersFromFile(const QString& filePath);
    void loadServersFromFile(const QString& filePath);
};

#endif
