/****************************************************************************
** Meta object code from reading C++ file 'balancer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "balancer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'balancer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Balancer_t {
    QByteArrayData data[12];
    char stringdata0[137];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Balancer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Balancer_t qt_meta_stringdata_Balancer = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Balancer"
QT_MOC_LITERAL(1, 9, 16), // "addListOfServers"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 13), // "QList<Server>"
QT_MOC_LITERAL(4, 41, 4), // "list"
QT_MOC_LITERAL(5, 46, 20), // "startQueryingServers"
QT_MOC_LITERAL(6, 67, 14), // "addValidServer"
QT_MOC_LITERAL(7, 82, 12), // "QHostAddress"
QT_MOC_LITERAL(8, 95, 4), // "addr"
QT_MOC_LITERAL(9, 100, 4), // "port"
QT_MOC_LITERAL(10, 105, 15), // "queryNextServer"
QT_MOC_LITERAL(11, 121, 15) // "quitApplication"

    },
    "Balancer\0addListOfServers\0\0QList<Server>\0"
    "list\0startQueryingServers\0addValidServer\0"
    "QHostAddress\0addr\0port\0queryNextServer\0"
    "quitApplication"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Balancer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       5,    0,   42,    2, 0x08 /* Private */,
       6,    2,   43,    2, 0x08 /* Private */,
      10,    0,   48,    2, 0x08 /* Private */,
      11,    0,   49,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7, QMetaType::UShort,    8,    9,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Balancer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Balancer *_t = static_cast<Balancer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->addListOfServers((*reinterpret_cast< const QList<Server>(*)>(_a[1]))); break;
        case 1: _t->startQueryingServers(); break;
        case 2: _t->addValidServer((*reinterpret_cast< const QHostAddress(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 3: _t->queryNextServer(); break;
        case 4: _t->quitApplication(); break;
        default: ;
        }
    }
}

const QMetaObject Balancer::staticMetaObject = {
    { &QCoreApplication::staticMetaObject, qt_meta_stringdata_Balancer.data,
      qt_meta_data_Balancer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Balancer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Balancer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Balancer.stringdata0))
        return static_cast<void*>(const_cast< Balancer*>(this));
    return QCoreApplication::qt_metacast(_clname);
}

int Balancer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCoreApplication::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
