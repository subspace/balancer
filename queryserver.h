#ifndef _QUERYSERVER_H_
#define _QUERYSERVER_H_

#include <QObject>

class QHostAddress;
class QUdpSocket;
class QTimer;

// This will query and return only servers that are supported by the ServeMe bots.
class QueryServer: public QObject {
    Q_OBJECT
public:
    QueryServer(QObject *parent = 0);
    ~QueryServer();

    void query(const QHostAddress& addr, quint16 port);

signals:
    void receivedValidServer(const QHostAddress& addr, quint16 port); /* A valid server is a server supported by the serveme bots */
    void finished();            /* Emited when all the queries have finished */
    
private slots:
    void parseServerInfo();
    void socketTimedOut();
    
private:
    QList<QUdpSocket *> activeQueries;
    QList<QTimer *> activeTimers;

    void removeTimerBySocket(const QUdpSocket* socket);
};

#endif
