#include "balancer.h"

int main(int argc, char **argv)
{
    Balancer a(argc, argv);
    a.start();
    return a.exec();
}
