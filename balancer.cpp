#include "balancer.h"
#include "queryserver.h"
#include <QHostInfo>
#include <QFile>
#include <QSettings>
#include <QTimer>

Balancer::Balancer(int &argc, char **argv):
    QCoreApplication(argc, argv)
{
}

Balancer::~Balancer()
{
}

void Balancer::loadBlackListFromFile(const QString& filePath)
{
    QFile f(filePath);
    if (!f.open(QFile::ReadOnly)) {
        qDebug() << "[!]" << f.fileName() << "not found.";
        return;
    }

    while (!f.atEnd()) {
        QByteArray line  = f.readLine();
        Server s;
        QList<QByteArray> p = line.split(':');

        s.addr = QHostAddress(QString(p.first()));
        s.port = p.last().trimmed().toUShort();

        blackListedServers.push_back(s);
    }
    f.close();

    qDebug("[*] %i servers loaded from %s.", blackListedServers.size(), f.fileName().toLatin1().data());
}

void Balancer::loadMastersFromFile(const QString& filePath)
{
    QFile f(filePath);
    if (!f.open(QFile::ReadOnly)) {
        qDebug() << "[!]" << f.fileName() << "not found.";
        return;
    }

    QByteArray line;
    while (!f.atEnd()) {
        line = f.readLine();
        QList<QByteArray> addrPort = line.split(':');

        // Resolve host
        QHostInfo h = QHostInfo::fromName(QString(addrPort.at(0)));
        if (h.error() != QHostInfo::NoError) {
            continue;
        }

        Server s;
        s.addr = h.addresses().first();
        s.port = addrPort.at(1).trimmed().toUShort();

        masterServers.push_back(s);
    }
    f.close();
    qDebug("[*] %i master servers loaded from %s.", masterServers.size(), f.fileName().toLatin1().data());
}

void Balancer::loadServersFromFile(const QString &filePath)
{
    QFile f(filePath);
    if (!f.open(QFile::ReadOnly)) {
        qDebug() << "[!]" << f.fileName() << "not found.";
        return;
    }

    QByteArray line;
    while (!f.atEnd()) {
        line = f.readLine();
        QList<QByteArray> addrPort = line.split(':');

        Server s;

        s.addr = QHostAddress(QString(addrPort.at(0)));
        s.port = addrPort.at(1).trimmed().toUShort();

        allServers.push_back(s);
    }
    f.close();
    qDebug("[*] %i qw servers added from %s.", allServers.size(), f.fileName().toLatin1().data());
}

void Balancer::start()
{
    QueryMaster* m = new QueryMaster(this);
    connect(m, &QueryMaster::receivedList, this, &Balancer::addListOfServers);
    connect(m, &QueryMaster::finished, this, &Balancer::startQueryingServers); // Start querying the servers after we are finished with the masters
    loadBlackListFromFile("blacklist.txt");
    loadMastersFromFile("masters.txt");
    foreach (const Server &s, masterServers) {
        m->query(s.addr, s.port);
    }

    loadServersFromFile("servers.txt");
}

void Balancer::addValidServer(const QHostAddress &addr, quint16 port)
{
    Server s;
    s.addr = addr;
    s.port = port;

    validServers.push_back(s);
}

void Balancer::queryNextServer()
{
    if (!allServers.size()) {
        quitApplication();
        return;
    }

    QueryServer* q = qobject_cast<QueryServer *>(sender());
    const Server &s = allServers.at(0);
    q->query(s.addr, s.port);
    allServers.pop_front();
}

void Balancer::addListOfServers(const QList<Server> &list)
{
    int i = 0;
    // Filter the list of servers, don't add duplicate entries
    foreach (const Server &s, list) {
        if (allServers.contains(s)) {
            continue;
        }
        allServers.push_back(s);
        i++;
    }
    if (!i)
        return;
    qDebug("[*] %i new servers added from a master.", i);
}

// Query all the servers on the list to check which of them is a valid one
void Balancer::startQueryingServers()
{
    int filtered = 0;
    for (int i = 0; i < blackListedServers.size(); i++) {
        if (allServers.contains(blackListedServers.at(i))) {
            allServers.removeAll(blackListedServers.at(i));
            filtered++;
        }
    }
    if (filtered) {
        qDebug() << "[*]" << filtered << "blacklisted servers filtered.";
    }

    qDebug() << "[*] Starting to query all servers.";
    QueryServer *q = new QueryServer(this);
    connect(q, &QueryServer::receivedValidServer, this, &Balancer::addValidServer);
    connect(q, &QueryServer::finished, this, &Balancer::queryNextServer);

    const Server &s1 = allServers.at(0);
    q->query(s1.addr, s1.port);
    allServers.pop_front();
}

void Balancer::quitApplication()
{
    generateConfig();
    qDebug() << "[*] Quitting...";
    QTimer::singleShot(1000, this, SLOT(quit()));
}

void Balancer::generateConfig()
{
    qDebug("[*] Generating new config file... ");

    QSettings in(QCoreApplication::applicationDirPath() + "/cimsqwbot.cfg", QSettings::IniFormat);
    QSettings out(QCoreApplication::applicationDirPath() + "/new_cimsqwbot.cfg", QSettings::IniFormat);

    out.setValue("quakeFolder", in.value("quakeFolder", QCoreApplication::applicationDirPath()).toString());
    out.setValue("botName", in.value("botName", "[ServeMe]").toString());
    out.setValue("botPing", in.value("botPing", 666).toInt());
    out.setValue("botTopColor", in.value("botTopColor", 11).toInt());
    out.setValue("botBottomColor", in.value("botBottomColor", 12).toInt());
    out.setValue("botSpectator", in.value("botSpectator", true).toBool());
    out.setValue("floodProtTime", qBound<int>(6,  in.value("floodProtTime", 6).toInt(), 9999));
    out.setValue("queryInterval", in.value("queryInterval", 1000).toInt());
    out.setValue("qwFloodProtTime", in.value("qwFloodProtTime", 600).toInt());
    out.setValue("spamFloodProtTime", in.value("spamFloodProtTime", 300).toUInt());
    out.setValue("timeToSayHiAfterConnected", in.value("timeToSayHiAfterConnected", 7).toInt());
    out.setValue("timeToWaitForCountReply", in.value("timeToWaitForCountReply", 7).toInt());
    out.setValue("developerMode", in.value("developerMode", false).toBool());
    out.setValue("sshHostName", in.value("sshHostName", "b4r.org").toString());
    out.setValue("sshUserName", in.value("sshUserName", "stomp").toString());
    out.setValue("sshPort", in.value("sshPort", 22).toUInt());
    out.setValue("refreshHostNamesHour", in.value("refreshHostNamesHour", 21).toInt());
    out.setValue("maxServers", in.value("maxServers", 100).toInt());

    out.beginWriteArray("servers");
    int i;
    for (i = 0; i < validServers.size(); ++i) {
        out.setArrayIndex(i);
        out.setValue("address", QHostAddress(validServers.at(i).addr.toIPv4Address()).toString() + ":" + QString::number(validServers.at(i).port) + ":");
    }
    out.endArray();

    if (i > out.value("maxServers").toInt())
        out.setValue("maxServers", i);

    qDebug("[*] Done!");
}
